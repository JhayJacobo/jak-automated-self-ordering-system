-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2020 at 12:51 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asos`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbladmin`
--

CREATE TABLE `tbladmin` (
  `id` int(10) NOT NULL,
  `position` varchar(12) NOT NULL,
  `lastname` varchar(12) NOT NULL,
  `firstname` varchar(12) NOT NULL,
  `username` varchar(12) NOT NULL,
  `password` varchar(12) NOT NULL,
  `status` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbladmin`
--

INSERT INTO `tbladmin` (`id`, `position`, `lastname`, `firstname`, `username`, `password`, `status`) VALUES
(1, 'Admin', 'Doe', 'John', 'admin', 'admin', 'Active'),
(2, 'Staff', 'Iso', 'Rafael', 'rafraf', 'rafaelpogi', 'Inactive'),
(7, 'Staff', 'Canlas', 'Red', 'redred', 'ready', 'Active'),
(8, 'Staff', 'try', 'try', 'admintry', 'admintry', 'Active'),
(9, 'Staff', 'au', 'au', 'auauau', 'auauau', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `tblcustomeraccounts`
--

CREATE TABLE `tblcustomeraccounts` (
  `id` int(12) NOT NULL,
  `lastname` varchar(12) NOT NULL,
  `firstname` varchar(12) NOT NULL,
  `contact` varchar(11) NOT NULL,
  `gender` varchar(12) NOT NULL,
  `course` varchar(12) NOT NULL,
  `year` varchar(12) NOT NULL,
  `position` varchar(12) NOT NULL,
  `status` varchar(12) NOT NULL,
  `reg` varchar(12) NOT NULL,
  `bday` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblcustomeraccounts`
--

INSERT INTO `tblcustomeraccounts` (`id`, `lastname`, `firstname`, `contact`, `gender`, `course`, `year`, `position`, `status`, `reg`, `bday`) VALUES
(123456789, 'Burayag', 'Biel', '09123456789', 'Female', '', '', 'Employee', 'Active', 'Registered', '1985-09-10'),
(2018010127, 'Jacobo', 'Jefferson', '09164883723', 'Male', 'BSIT', '2nd', 'Student', 'Active', 'Registered', '1997-09-13'),
(2018010142, 'Bautista', 'Kaila', '09123456789', 'Female', 'BSIT', '2nd', 'Student', 'Active', 'Registered', '2000-05-20'),
(2018010271, 'Morata', 'Nalaine', '09987654321', 'Female', 'ACT', '2nd', 'Student', 'Active', 'Registered', '1987-05-24');

-- --------------------------------------------------------

--
-- Table structure for table `tblmenu`
--

CREATE TABLE `tblmenu` (
  `foodID` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `meal` varchar(12) NOT NULL,
  `type` varchar(12) NOT NULL,
  `qty` int(255) NOT NULL,
  `price` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblmenu`
--

INSERT INTO `tblmenu` (`foodID`, `name`, `meal`, `type`, `qty`, `price`) VALUES
(1, 'Egg', 'Breakfast', 'Meat', 10, 10),
(2, 'Hotdog', 'Breakfast', 'Meat', 10, 8),
(3, 'Rice', 'addons', 'Wheat', 12, 12),
(4, 'Water', 'addons', 'Drinks', 18, 13),
(5, 'Coke', 'addons', 'Drinks', 10, 15);

-- --------------------------------------------------------

--
-- Table structure for table `tblorder`
--

CREATE TABLE `tblorder` (
  `queueID` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `list` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `sub` varchar(255) NOT NULL,
  `Total` int(255) NOT NULL,
  `status` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblorder`
--

INSERT INTO `tblorder` (`queueID`, `name`, `list`, `price`, `qty`, `sub`, `Total`, `status`) VALUES
(1, 'Jacobo Jefferson', 'Coke\r\nWater\r\nRice\r\n', '15\r\n13\r\n12\r\n', '1\r\n2\r\n3\r\n', '15\r\n26\r\n36\r\n', 77, ''),
(2, 'Jacobo Jefferson', 'Egg\r\n', '10\r\n', '4\r\n', '40\r\n', 40, ''),
(3, 'Jacobo Jefferson', 'Hotdog\r\nRice\r\nWater\r\n', '8\r\n12\r\n13\r\n', '2\r\n4\r\n4\r\n', '16\r\n48\r\n52\r\n', 116, ''),
(4, 'Jacobo Jefferson', 'Water\r\nCoke\r\n', '13\r\n15\r\n', '2\r\n2\r\n', '26\r\n30\r\n', 56, ''),
(5, 'Jacobo Jefferson', 'Rice\r\nWater\r\nCoke\r\n', '12\r\n13\r\n15\r\n', '2\r\n2\r\n2\r\n', '24\r\n26\r\n30\r\n', 80, ''),
(6, 'Jacobo Jefferson', 'Egg\r\nHotdog\r\nRice\r\nWater\r\nCoke\r\n', '10\r\n8\r\n12\r\n13\r\n15\r\n', '3\r\n2\r\n3\r\n5\r\n2\r\n', '30\r\n16\r\n36\r\n65\r\n30\r\n', 177, ''),
(7, 'Jacobo Jefferson', 'Egg\r\n', '10\r\n', '2\r\n', '20\r\n', 95, '');

-- --------------------------------------------------------

--
-- Table structure for table `tblpaid`
--

CREATE TABLE `tblpaid` (
  `id` int(255) NOT NULL,
  `queueID` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `list` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `sub` varchar(255) NOT NULL,
  `Total` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbladmin`
--
ALTER TABLE `tbladmin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblcustomeraccounts`
--
ALTER TABLE `tblcustomeraccounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblmenu`
--
ALTER TABLE `tblmenu`
  ADD PRIMARY KEY (`foodID`);

--
-- Indexes for table `tblorder`
--
ALTER TABLE `tblorder`
  ADD PRIMARY KEY (`queueID`);

--
-- Indexes for table `tblpaid`
--
ALTER TABLE `tblpaid`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbladmin`
--
ALTER TABLE `tbladmin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tblmenu`
--
ALTER TABLE `tblmenu`
  MODIFY `foodID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tblpaid`
--
ALTER TABLE `tblpaid`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
