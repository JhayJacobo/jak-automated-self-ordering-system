﻿Imports MySql.Data.MySqlClient
Public Class Login
    Function LoginUser()
        Dim reload As New OrderFoodMenu
        If (TextBox1.Text IsNot "") Then
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection

                .CommandText = "SELECT * FROM tblcustomeraccounts where id LIKE '" & TextBox1.Text & "'"
                dr = cmd.ExecuteReader()
                If dr.HasRows = True Then
                    While dr.Read()
                        reg = dr.GetValue(9).ToString()
                        If reg = "Registered" Then
                            Me.Hide()
                            OrderFoodMenu.Show()
                        Else
                            MessageBox.Show("Signup to Activate your Account ID", "ACTIVATE", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                        End If

                    End While
                Else

                    MessageBox.Show(" ID Not found, Please Try again", "ID", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Application.Restart()
                End If

            End With
        Else
            MessageBox.Show("Please Scan your ID", "SCAN", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Function

    Private Sub Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '   TextBox1.Select()
        TextBox1.Focus()

        Me.LinkLabel2.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
    End Sub

    Private Sub Button14_Click(sender As Object, e As EventArgs)
        Me.Hide()
        CustomerSignUp.Show()
    End Sub

    Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        Dim reload As New KeyLogin
        Me.Hide()
        reload.Show()
    End Sub

    Private Sub BunifuThinButton21_Click(sender As Object, e As EventArgs) Handles BunifuThinButton21.Click
        Me.Hide()
        CustomerSignUp.Show()
    End Sub

    Private Sub BunifuMetroTextbox1_KeyDown(sender As Object, e As KeyEventArgs) 
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            LoginUser()

        End If
    End Sub

    Private Sub TextBox1_KeyDown(sender As Object, e As KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            LoginUser()
            TextBox1.Focus()
            TextBox1.SelectAll()
        End If
    End Sub
End Class