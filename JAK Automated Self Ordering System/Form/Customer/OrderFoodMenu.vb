﻿Imports MySql.Data.MySqlClient
Public Class OrderFoodMenu
    Public foodList As New ListViewItem
    Public Newdr As MySqlDataReader
    Dim alltotal As Decimal
    Dim sum As Decimal
    Dim que As Integer
    Dim keyid As String
    Dim keyname As String
    Dim lastnameLabel As String
    Dim firstnameLabel As String
    Dim row_qty As Decimal
    Dim row_price As Decimal
    Dim qty_integer As Integer
    Dim price_integer As Decimal
    Dim check As String
    Dim counter As Integer
    Dim mysql_result
    Dim lv1_qty, lv2_qty As Integer
    Dim customerName
    Dim positions

    Private Sub OrderFoodMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RadioButton1.Checked = True
        'ListView1.Columns.c
        'connect()
        'cmd.Connection = con
        'cmd.CommandText = "Select * from tblmenu where qty > 0 "
        'dr = cmd.ExecuteReader
        'If dr.HasRows = True Then
        '    While dr.Read
        '        foodList = ListView1.Items.Add(dr.GetValue(1).ToString)
        '        foodList.SubItems.Add(dr.GetValue(2).ToString)
        '        foodList.SubItems.Add(dr.GetValue(3).ToString)
        '        foodList.SubItems.Add(dr.GetValue(4).ToString)
        '        foodList.SubItems.Add(dr.GetValue(5).ToString)
        '    End While
        'End If

        connect()
        cmd.Connection = con
        cmd.CommandText = "Select * from tblcustomeraccounts where id LIKE '" & KeyLogin.TextBox1.Text & "' or  id LIKE '" & Login.TextBox1.Text & "'"
        Newdr = cmd.ExecuteReader
        If Newdr.HasRows = True Then
            While Newdr.Read
                idenLabel = Newdr.GetValue(0).ToString()
                lastnameLabel = Newdr.GetValue(1).ToString()
                firstnameLabel = Newdr.GetValue(2).ToString()
                positions = Newdr.GetValue(7).ToString()
                Label1.Text = lastnameLabel + " " + firstnameLabel
                Label2.Text = idenLabel
            End While
        End If
    End Sub

    ' Implements a comparer for ListView columns.
    Class ListViewComparer
        Implements IComparer

        Private m_ColumnNumber As Integer
        Private m_SortOrder As SortOrder

        Public Sub New(ByVal column_number As Integer, ByVal _
        sort_order As SortOrder)
            m_ColumnNumber = column_number
            m_SortOrder = sort_order
        End Sub

        ' Compare the items in the appropriate column
        ' for objects x and y.
        Public Function Compare(ByVal x As Object, ByVal y As _
        Object) As Integer Implements _
        System.Collections.IComparer.Compare
            Dim item_x As ListViewItem = DirectCast(x,
            ListViewItem)
            Dim item_y As ListViewItem = DirectCast(y,
            ListViewItem)

            ' Get the sub-item values.
            Dim string_x As String
            If item_x.SubItems.Count <= m_ColumnNumber Then
                string_x = ""
            Else
                string_x = item_x.SubItems(m_ColumnNumber).Text
            End If

            Dim string_y As String
            If item_y.SubItems.Count <= m_ColumnNumber Then
                string_y = ""
            Else
                string_y = item_y.SubItems(m_ColumnNumber).Text
            End If

            ' Compare them.
            If m_SortOrder = SortOrder.Ascending Then
                If IsNumeric(string_x) And IsNumeric(string_y) _
                Then
                    Return Val(string_x).CompareTo(Val(string_y))
                ElseIf IsDate(string_x) And IsDate(string_y) _
                Then
                    Return DateTime.Parse(string_x).CompareTo(DateTime.Parse(string_y))
                Else
                    Return String.Compare(string_x, string_y)
                End If
            Else
                If IsNumeric(string_x) And IsNumeric(string_y) _
                Then
                    Return Val(string_y).CompareTo(Val(string_x))
                ElseIf IsDate(string_x) And IsDate(string_y) _
                Then
                    Return DateTime.Parse(string_y).CompareTo(DateTime.Parse(string_x))
                Else
                    Return String.Compare(string_y, string_x)
                End If
            End If
        End Function
    End Class

    ' The column currently used for sorting.
    Private m_SortingColumn As ColumnHeader

    ' Sort using the clicked column.
    Private Sub lvwBooks_ColumnClick(ByVal sender As _
    System.Object, ByVal e As _
    System.Windows.Forms.ColumnClickEventArgs) Handles _
    ListView1.ColumnClick
        ' Get the new sorting column.
        Dim new_sorting_column As ColumnHeader =
        ListView1.Columns(e.Column)

        ' Figure out the new sorting order.
        Dim sort_order As System.Windows.Forms.SortOrder
        If m_SortingColumn Is Nothing Then
            ' New column. Sort ascending.
            sort_order = SortOrder.Ascending
        Else
            ' See if this is the same column.
            If new_sorting_column.Equals(m_SortingColumn) Then
                ' Same column. Switch the sort order.
                If m_SortingColumn.Text.StartsWith("> ") Then
                    sort_order = SortOrder.Descending
                Else
                    sort_order = SortOrder.Ascending
                End If
            Else
                ' New column. Sort ascending.
                sort_order = SortOrder.Ascending
            End If

            ' Remove the old sort indicator.
            m_SortingColumn.Text =
            m_SortingColumn.Text.Substring(2)
        End If

        ' Display the new sort order.
        m_SortingColumn = new_sorting_column
        If sort_order = SortOrder.Ascending Then
            m_SortingColumn.Text = "> " & m_SortingColumn.Text
        Else
            m_SortingColumn.Text = "< " & m_SortingColumn.Text
        End If

        ' Create a comparer.
        ListView1.ListViewItemSorter = New _
        ListViewComparer(e.Column, sort_order)

        ' Sort.

        ListView1.Sort()
    End Sub



    Private Sub ListView1_Click(sender As Object, e As EventArgs) Handles ListView1.Click
        Dim name, qty, price As String
        Dim lv2qty

        Dim IsItemExist As Boolean = False

        name = ListView1.Items(ListView1.FocusedItem.Index).SubItems(0).Text
        qty = ListView1.Items(ListView1.FocusedItem.Index).SubItems(3).Text
        price = ListView1.Items(ListView1.FocusedItem.Index).SubItems(4).Text

        'For x = 0 To ListView1.Items.Count - 1
        'lv2qty = ListView2.Items(0).SubItems(1).Text
        'Next


        Dim valuefromuser As Integer = +1
        'If lv2qty > qty Then

        '    MessageBox.Show("Exceed Quantity!", "Quantity", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    valuefromuser = -1
        'Else
        valuefromuser = Convert.ToInt32(valuefromuser)

            lv1_qty = ListView1.Items(ListView1.FocusedItem.Index).SubItems(3).Text

            For x As Integer = 0 To ListView2.Items.Count - 1
                lv2_qty = ListView2.Items(x).SubItems(1).Text
            Next

            If valuefromuser > lv1_qty Then
                MessageBox.Show("should not exceed to Qty stocks", "Quantity", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Else
            If valuefromuser > 0 Then
                ListView1.Items(ListView1.FocusedItem.Index).SubItems(3).Text -= 1
                For Each lv2_item As ListViewItem In ListView2.Items
                    Dim check_item As String

                    check_item = ListView2.Items(lv2_item.Index).SubItems(0).Text
                    If (name = check_item) Then
                        IsItemExist = True
                        ListView2.Items(lv2_item.Index).SubItems(1).Text = Convert.ToInt32(ListView2.Items(lv2_item.Index).SubItems(1).Text) + (valuefromuser)
                    End If
                Next

                If (ListView2.Items.Count = 0) Then
                    'no order in table
                    Dim list_view_selected_item As New ListViewItem(name)
                    list_view_selected_item.SubItems.Add(valuefromuser)
                    list_view_selected_item.SubItems.Add(price)
                    list_view_selected_item.SubItems.Add(price * valuefromuser)
                    ListView2.Items.Add(list_view_selected_item)
                ElseIf (IsItemExist = False) Then
                    'item doesn't exist 
                    Dim list_view_selected_item As New ListViewItem(name)
                    list_view_selected_item.SubItems.Add(valuefromuser)
                    list_view_selected_item.SubItems.Add(price)
                    list_view_selected_item.SubItems.Add(price * valuefromuser)
                    ListView2.Items.Add(list_view_selected_item)
                End If

            End If

            Dim main_total As Decimal
                For Each item As ListViewItem In ListView2.Items
                    row_qty = ListView2.Items(item.Index).SubItems(1).Text
                    row_price = ListView2.Items(item.Index).SubItems(2).Text
                    qty_integer = Decimal.Parse(row_qty)
                    price_integer = Decimal.Parse(row_price)
                    sum = qty_integer * price_integer
                    ListView2.Items(item.Index).SubItems(3).Text = sum
                    main_total += Convert.ToDecimal(ListView2.Items(item.Index).SubItems(3).Text)

                Next
                total_price_txt.Text = main_total
                alltotal = main_total
            End If
        'End If
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click
        Label2.Text = Login.TextBox1.Text
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        ListView1.Items.Clear()
        RadioButton1.Checked = True
        Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
        mySqlConnection.Open()
        With cmd
            .Connection = mySqlConnection
            .CommandText = "SELECT * FROM tblmenu where name LIKE '%" & TextBox1.Text & "%' and qty > 0 "
            dr = cmd.ExecuteReader()
            If dr.HasRows = True Then
                While dr.Read()
                    foodList = ListView1.Items.Add(dr.GetValue(1).ToString())
                    foodList.SubItems.Add(dr.GetValue(2).ToString())
                    foodList.SubItems.Add(dr.GetValue(3).ToString())
                    foodList.SubItems.Add(dr.GetValue(4).ToString())
                    foodList.SubItems.Add(dr.GetValue(5).ToString())
                End While
            End If
        End With
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim result As DialogResult
        Dim reload As New Login
        Dim List As String
        Dim price As String
        Dim qty As String
        Dim subtotal As String
        Dim lv2_item_name, lv1_item_name As String
        Dim lv2_item_qty, lv1_item_qty As Integer
        Dim queueNumber

        'Dim themySqlConnection As MySqlConnection = StartMySqlConnection()
        'themySqlConnection.Open()
        'With cmd
        '    .Connection = themySqlConnection
        '    .CommandText = "INSERT INTO tblorder (`queueID`,`name`) VALUES (0,'.')"
        '    Dim mysql_result = cmd.ExecuteNonQuery
        'End With
        Dim lv2_item As ListViewItem
        For Each lv2_item In ListView2.Items
            lv2_item_name = ListView2.Items(lv2_item.Index).SubItems(0).Text
            For Each lv1_item As ListViewItem In ListView1.Items
                lv1_item_name = ListView1.Items(lv1_item.Index).SubItems(0).Text
                If (lv2_item_name = lv1_item_name) Then

                    lv1_item_qty = ListView1.Items(lv1_item.Index).SubItems(3).Text
                    lv2_item_qty = ListView2.Items(lv2_item.Index).SubItems(1).Text
                End If
            Next


        Next

        If ListView2.Items(lv2_item.Index).SubItems(1).Text = 0 Then
            MessageBox.Show("Please input Order", "ORDER", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            If ListView2.Items.Count = 0 Then
                MessageBox.Show("Please Order", "ORDER MENU", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Else
                If (lv2_item_qty <= lv1_item_qty) Then
                    Dim lv2qty
                    For Each z As ListViewItem In ListView2.Items
                        lv2qty = ListView2.Items(z.Index).SubItems(1).Text
                        For Each x As ListViewItem In ListView1.Items
                            Dim renewSqlConnection As MySqlConnection = StartMySqlConnection()
                            renewSqlConnection.Open()
                            With cmd
                                .Connection = renewSqlConnection
                                Dim foodname = ListView1.Items(x.Index).SubItems(0).Text
                                Dim foodname2 = ListView2.Items(z.Index).SubItems(0).Text
                                Dim lv1qty = ListView1.Items(x.Index).SubItems(3).Text

                                If foodname = foodname2 Then
                                    Dim newqty As Integer = lv1qty - lv2qty
                                    .CommandText = "Update tblmenu set qty='" & newqty & "' Where name = '" & foodname & "'"
                                    mysql_result = cmd.ExecuteReader
                                End If
                            End With
                        Next
                    Next

                    Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
                    mySqlConnection.Open()
                    With cmd
                        .Connection = mySqlConnection

                        .CommandText = "SELECT * FROM tblorder ORDER by 1 desc limit 1"
                        dr = cmd.ExecuteReader()
                        If dr.HasRows = True Then
                            While dr.Read()
                                reg = dr.Item("queueID")
                                counter += 1
                                queueNumber = reg + counter
                                result = MessageBox.Show("Do you wish to proceed?", "Ask", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                                If result = DialogResult.Yes Then
                                    con = StartMySqlConnection()
                                    con.Open()
                                    With cmd
                                        customerName = Label1.Text
                                        For x As Integer = 0 To ListView2.Items.Count - 1
                                            List = ListView2.Items(x).SubItems(0).Text + vbNewLine
                                            qty = ListView2.Items(x).SubItems(1).Text + vbNewLine
                                            price = ListView2.Items(x).SubItems(2).Text + vbNewLine
                                            subtotal = ListView2.Items(x).SubItems(3).Text + vbNewLine
                                            price_total = total_price_txt.Text
                                            .Connection = con
                                            .CommandText = "INSERT INTO tblorder (`queueID`,`name`,`list`,`price`,`qty`,`sub`,`Total`) VALUES (" & queueNumber & ",'" & customerName & "','" & List & "','" & price & "','" & qty & "','" & subtotal & "', '" & price_total & "')"
                                            mysql_result = cmd.ExecuteNonQuery
                                        Next

                                        Dim mynewSqlConnection As MySqlConnection = StartMySqlConnection()
                                        mynewSqlConnection.Open()
                                        With cmd
                                            .Connection = mynewSqlConnection
                                            .CommandText = "Update tblprint Set id =  " & queueNumber & "  "
                                            mysql_result = cmd.ExecuteNonQuery
                                        End With

                                        If mysql_result = 0 Then
                                            MsgBox("Order Process Failed", vbCritical)
                                        Else
                                            MessageBox.Show("Order successfully" & vbNewLine & "Queue Number : " & queueNumber, "ORDER", MessageBoxButtons.OK, MessageBoxIcon.Information)

                                            Dim awSqlConnection As MySqlConnection = StartMySqlConnection()
                                            awSqlConnection.Open()
                                            With cmd
                                                '.Connection = awSqlConnection
                                                .CommandText = "INSERT INTO tblfinalorder (`queue_id`,`name`,`Total`,`position`) VALUES (" & queueNumber & ",'" & customerName & "', '" & price_total & "', '" & positions & "')"
                                                mysql_result = cmd.ExecuteNonQuery
                                            End With
                                        End If
                                    End With
                                    Dim print As New ReceiptPrint
                                    print.Load()
                                    print.Refresh()
                                    print.PrintToPrinter(1, False, 0, 0)

                                    Me.Hide()
                                    Application.Restart()
                                End If
                            End While
                        End If
                    End With


                Else
                    MessageBox.Show("should not exceed to Qty stocks", "Quantity", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            End If
        End If





    End Sub

    'Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
    '    If ListView2.SelectedItems.Count = 0 Then
    '        MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    Else
    '        Dim result As String
    '        result = MessageBox.Show("Are you sure you want to Delete this record? ", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
    '        If result = DialogResult.Yes Then

    '            For Each item As ListViewItem In ListView2.SelectedItems

    '                Dim minus As Integer

    '                minus = ListView2.Items(item.Index).SubItems(3).Text

    '                alltotal -= minus
    '                total_price_txt.Text = alltotal
    '                item.Remove()

    '            Next
    '        End If
    '    End If
    'End Sub

    Private Sub BunifuImageButton1_Click(sender As Object, e As EventArgs) Handles BunifuImageButton1.Click
        Application.Restart()
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        ListView1.Items.Clear()

        If RadioButton2.Checked = True Then
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection
                .CommandText = "SELECT * FROM tblmenu where meal LIKE 'Breakfast' and qty > 0 "
                dr = cmd.ExecuteReader()
                If dr.HasRows = True Then
                    While dr.Read()
                        foodList = ListView1.Items.Add(dr.GetValue(1).ToString())
                        foodList.SubItems.Add(dr.GetValue(2).ToString())
                        foodList.SubItems.Add(dr.GetValue(3).ToString())
                        foodList.SubItems.Add(dr.GetValue(4).ToString())
                        foodList.SubItems.Add(dr.GetValue(5).ToString())
                    End While
                End If
            End With
        Else
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection
                .CommandText = "SELECT * FROM tblmenu "
                dr = cmd.ExecuteReader()
                If dr.HasRows = True Then
                    While dr.Read()
                        foodList = ListView1.Items.Add(dr.GetValue(1).ToString())
                        foodList.SubItems.Add(dr.GetValue(2).ToString())
                        foodList.SubItems.Add(dr.GetValue(3).ToString())
                        foodList.SubItems.Add(dr.GetValue(4).ToString())
                        foodList.SubItems.Add(dr.GetValue(5).ToString())
                    End While
                End If
            End With

        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged
        ListView1.Items.Clear()

        If RadioButton3.Checked = True Then
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection
                .CommandText = "SELECT * FROM tblmenu where meal LIKE 'Lunch' and qty > 0"
                dr = cmd.ExecuteReader()
                If dr.HasRows = True Then
                    While dr.Read()
                        foodList = ListView1.Items.Add(dr.GetValue(1).ToString())
                        foodList.SubItems.Add(dr.GetValue(2).ToString())
                        foodList.SubItems.Add(dr.GetValue(3).ToString())
                        foodList.SubItems.Add(dr.GetValue(4).ToString())
                        foodList.SubItems.Add(dr.GetValue(5).ToString())
                    End While
                End If
            End With
        Else
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection
                .CommandText = "SELECT * FROM tblmenu "
                dr = cmd.ExecuteReader()
                If dr.HasRows = True Then
                    While dr.Read()
                        foodList = ListView1.Items.Add(dr.GetValue(1).ToString())
                        foodList.SubItems.Add(dr.GetValue(2).ToString())
                        foodList.SubItems.Add(dr.GetValue(3).ToString())
                        foodList.SubItems.Add(dr.GetValue(4).ToString())
                        foodList.SubItems.Add(dr.GetValue(5).ToString())
                    End While
                End If
            End With

        End If
    End Sub

    Private Sub RadioButton4_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton4.CheckedChanged
        ListView1.Items.Clear()

        If RadioButton4.Checked = True Then
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection
                .CommandText = "SELECT * FROM tblmenu where meal LIKE 'Snacks' and qty > 0"
                dr = cmd.ExecuteReader()
                If dr.HasRows = True Then
                    While dr.Read()
                        foodList = ListView1.Items.Add(dr.GetValue(1).ToString())
                        foodList.SubItems.Add(dr.GetValue(2).ToString())
                        foodList.SubItems.Add(dr.GetValue(3).ToString())
                        foodList.SubItems.Add(dr.GetValue(4).ToString())
                        foodList.SubItems.Add(dr.GetValue(5).ToString())
                    End While
                End If
            End With
        Else
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection
                .CommandText = "SELECT * FROM tblmenu "
                dr = cmd.ExecuteReader()
                If dr.HasRows = True Then
                    While dr.Read()
                        foodList = ListView1.Items.Add(dr.GetValue(1).ToString())
                        foodList.SubItems.Add(dr.GetValue(2).ToString())
                        foodList.SubItems.Add(dr.GetValue(3).ToString())
                        foodList.SubItems.Add(dr.GetValue(4).ToString())
                        foodList.SubItems.Add(dr.GetValue(5).ToString())
                    End While
                End If
            End With

        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        ListView1.Items.Clear()

        If RadioButton1.Checked = True Then
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection
                .CommandText = "SELECT * FROM tblmenu where qty > 0"
                dr = cmd.ExecuteReader()
                If dr.HasRows = True Then
                    While dr.Read()
                        foodList = ListView1.Items.Add(dr.GetValue(1).ToString())
                        foodList.SubItems.Add(dr.GetValue(2).ToString())
                        foodList.SubItems.Add(dr.GetValue(3).ToString())
                        foodList.SubItems.Add(dr.GetValue(4).ToString())
                        foodList.SubItems.Add(dr.GetValue(5).ToString())
                    End While
                End If
            End With
        Else
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection
                .CommandText = "SELECT * FROM tblmenu "
                dr = cmd.ExecuteReader()
                If dr.HasRows = True Then
                    While dr.Read()
                        foodList = ListView1.Items.Add(dr.GetValue(1).ToString())
                        foodList.SubItems.Add(dr.GetValue(2).ToString())
                        foodList.SubItems.Add(dr.GetValue(3).ToString())
                        foodList.SubItems.Add(dr.GetValue(4).ToString())
                        foodList.SubItems.Add(dr.GetValue(5).ToString())
                    End While
                End If
            End With
        End If
    End Sub

    Private Sub ListView2_MouseClick(sender As Object, e As MouseEventArgs) Handles ListView2.MouseClick
        'Dim lv2_item_name, lv1_item_name As String
        'Dim lv2_item_qty, lv1_item_qty As Integer


        'For Each lv1_item As ListViewItem In ListView1.Items
        '    lv1_item_name = ListView1.Items(lv1_item.Index).SubItems(0).Text
        '    For Each lv2_item As ListViewItem In ListView2.Items
        '        lv2_item_name = ListView2.Items(lv2_item.Index).SubItems(0).Text
        '        If (lv2_item_name = lv1_item_name) Then

        '            lv1_item_qty = ListView1.Items(lv1_item.Index).SubItems(3).Text
        '            lv2_item_qty = ListView2.Items(ListView2.FocusedItem.Index).SubItems(1).Text
        '            MessageBox.Show(lv1_item_name & " " & lv2_item_name)
        '        End If
        '    Next
        'Next


        ''Dim price As Decimal = ListView1.Items(ListView1.FocusedItem.Index).SubItems(4).Text
        ''If ListView2.Items(ListView2.FocusedItem.Index).SubItems(1).Text < 1 Then
        ''    Me.ListView2.Items.RemoveAt(Me.ListView2.FocusedItem.Index)
        ''Else
        ''    lv1_item_qty += 1
        ''    ListView2.Items(ListView2.FocusedItem.Index).SubItems(1).Text -= 1
        ''    ListView2.Items(ListView2.FocusedItem.Index).SubItems(3).Text -= price
        ''End If

        Dim lv1_item_name, lv2_item_name As String
        Dim lv2_item_qty, lv1_item_qty As Integer
        Dim nowprice As Decimal
        Dim price As Decimal
        For Each lv2_item As ListViewItem In ListView2.SelectedItems
            lv2_item_name = ListView2.Items(ListView2.FocusedItem.Index).SubItems(0).Text
            For Each lv1_item As ListViewItem In ListView1.Items
                price = ListView1.Items(lv1_item.Index).SubItems(4).Text
                lv1_item_name = ListView1.Items(lv1_item.Index).SubItems(0).Text

                If (lv1_item_name = lv2_item_name) Then
                    price = ListView1.Items(lv1_item.Index).SubItems(4).Text
                    ListView1.Items(lv1_item.Index).SubItems(3).Text += 1
                    ListView2.Items(ListView2.FocusedItem.Index).SubItems(1).Text -= 1

                    ListView2.Items(ListView2.FocusedItem.Index).SubItems(3).Text -= Convert.ToDecimal(price)
                    total_price_txt.Text -= Convert.ToDecimal(price)
                End If
            Next
        Next
    End Sub

    Private Sub ListView2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListView2.SelectedIndexChanged
        If ListView2.Items(ListView2.FocusedItem.Index).SubItems(1).Text < 1 Then
            Me.ListView2.Items.RemoveAt(Me.ListView2.FocusedItem.Index)
        End If
    End Sub

    'Private Sub ListView2_MouseDown(sender As Object, e As MouseEventArgs) Handles ListView2.MouseDown
    '    'Dim lv1_item_name As ListViewHitTestInfo = ListView1.HitTest(e.X, e.Y)
    '    'Dim lv2_item_name As ListViewHitTestInfo = ListView2.HitTest(e.X, e.Y)
    '    'MsgBox(info.SubItem.ToString())
    '    'If Not IsNothing(info.SubItem) Then
    '    'End If



    'End Sub

    Private Sub RadioButton5_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton5.CheckedChanged
        ListView1.Items.Clear()

        If RadioButton5.Checked = True Then
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection
                .CommandText = "SELECT * FROM tblmenu where meal LIKE 'addons' and qty > 0"
                dr = cmd.ExecuteReader()
                If dr.HasRows = True Then
                    While dr.Read()
                        foodList = ListView1.Items.Add(dr.GetValue(1).ToString())
                        foodList.SubItems.Add(dr.GetValue(2).ToString())
                        foodList.SubItems.Add(dr.GetValue(3).ToString())
                        foodList.SubItems.Add(dr.GetValue(4).ToString())
                        foodList.SubItems.Add(dr.GetValue(5).ToString())
                    End While
                End If
            End With
        Else
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection
                .CommandText = "SELECT * FROM tblmenu "
                dr = cmd.ExecuteReader()
                If dr.HasRows = True Then
                    While dr.Read()
                        foodList = ListView1.Items.Add(dr.GetValue(1).ToString())
                        foodList.SubItems.Add(dr.GetValue(2).ToString())
                        foodList.SubItems.Add(dr.GetValue(3).ToString())
                        foodList.SubItems.Add(dr.GetValue(4).ToString())
                        foodList.SubItems.Add(dr.GetValue(5).ToString())
                    End While
                End If
            End With
        End If
    End Sub


End Class