﻿Imports MySql.Data.MySqlClient
Public Class CustomerManagement
    Private Sub CustomerManagement_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            sql = "Select * from tblcustomeraccounts"
            connect()
            cmd = New MySqlCommand(sql, con)
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            ListView1.Items.Clear()
            Dim newlist As ListViewItem

            Do While dr.Read = True
                newlist = New ListViewItem(dr("id").ToString)
                newlist.SubItems.Add(dr("lastname"))
                newlist.SubItems.Add(dr("firstname"))
                newlist.SubItems.Add(dr("position"))
                newlist.SubItems.Add(dr("status"))
                ListView1.Items.Add(newlist)
            Loop

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cmd.Dispose()
            con.Close()
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)
        stat = ListView1.SelectedItems(0).SubItems(4).Text
        id = ListView1.SelectedItems(0).SubItems(0).Text
        Dim reload As New CustomerManagement
        If ListView1.SelectedItems.Count = 0 Then
            MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection

                If stat = "Active" Then
                    .CommandText = "Update tblcustomeraccounts set status= 'Inactive' Where id = " & id & ""
                    Dim mysql_result = cmd.ExecuteNonQuery
                    MessageBox.Show("Account Deactivated")
                    Me.Hide()
                    reload.Show()
                ElseIf stat = "Inactive" Then
                    .CommandText = "Update tblcustomeraccounts set status= 'Active' Where id = " & id & ""
                    Dim mysql_result = cmd.ExecuteNonQuery
                    MessageBox.Show("Account Activated")
                    Me.Hide()
                    reload.Show()
                End If

            End With
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)
        Dim result As DialogResult
        Dim frm1 As New CustomerManagement

        If ListView1.SelectedItems.Count = 0 Then
            MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            id = ListView1.SelectedItems(0).SubItems(0).Text
            result = MessageBox.Show("Are you sure you want to Delete this record? ", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Information)


            If result = DialogResult.Yes Then
                Dim newmySqlConnection As MySqlConnection = StartMySqlConnection()
                newmySqlConnection.Open()
                With cmd
                    .Connection = newmySqlConnection
                    .CommandText = "Delete from tblcustomeraccounts where id = " & id & ""
                    cmd.ExecuteNonQuery()
                    MessageBox.Show("Successfully record deleted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ListView1.Items.Clear()
                    con = StartMySqlConnection()
                    con.Open()
                    With cmd
                        .Connection = con
                        .CommandText = "Select * from tblcustomeraccounts"
                        dr = cmd.ExecuteReader
                        If dr.HasRows = True Then
                            While dr.Read
                                foodList = ListView1.Items.Add(dr.GetValue(0).ToString())
                                foodList.SubItems.Add(dr.GetValue(1).ToString())
                                foodList.SubItems.Add(dr.GetValue(2).ToString())
                                foodList.SubItems.Add(dr.GetValue(3).ToString())
                                foodList.SubItems.Add(dr.GetValue(4).ToString())
                                foodList.SubItems.Add(dr.GetValue(5).ToString())
                            End While
                            Me.Hide()
                            frm1.Show()
                        End If
                    End With
                End With
            Else
                Return
            End If
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs)
        Me.Hide()
        LoginControl.Show()
    End Sub

    Private Sub BunifuFlatButton1_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton1.Click
        stat = ListView1.SelectedItems(0).SubItems(4).Text
        id = ListView1.SelectedItems(0).SubItems(0).Text
        Dim reload As New CustomerManagement
        If ListView1.SelectedItems.Count = 0 Then
            MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection

                If stat = "Active" Then
                    .CommandText = "Update tblcustomeraccounts set status= 'Inactive' Where id = " & id & ""
                    Dim mysql_result = cmd.ExecuteNonQuery
                    MessageBox.Show("Account Deactivated")
                    Me.Hide()
                    reload.Show()
                ElseIf stat = "Inactive" Then
                    .CommandText = "Update tblcustomeraccounts set status= 'Active' Where id = " & id & ""
                    Dim mysql_result = cmd.ExecuteNonQuery
                    MessageBox.Show("Account Activated")
                    Me.Hide()
                    reload.Show()
                End If

            End With
        End If
    End Sub

    Private Sub BunifuFlatButton2_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton2.Click
        Dim result As DialogResult
        Dim frm1 As New CustomerManagement

        If ListView1.SelectedItems.Count = 0 Then
            MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            id = ListView1.SelectedItems(0).SubItems(0).Text
            result = MessageBox.Show("Are you sure you want to Delete this record? ", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Information)


            If result = DialogResult.Yes Then
                Dim newmySqlConnection As MySqlConnection = StartMySqlConnection()
                newmySqlConnection.Open()
                With cmd
                    .Connection = newmySqlConnection
                    .CommandText = "Delete from tblcustomeraccounts where id = " & id & ""
                    cmd.ExecuteNonQuery()
                    MessageBox.Show("Successfully record deleted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ListView1.Items.Clear()
                    con = StartMySqlConnection()
                    con.Open()
                    With cmd
                        .Connection = con
                        .CommandText = "Select * from tblcustomeraccounts"
                        dr = cmd.ExecuteReader
                        If dr.HasRows = True Then
                            While dr.Read
                                foodList = ListView1.Items.Add(dr.GetValue(0).ToString())
                                foodList.SubItems.Add(dr.GetValue(1).ToString())
                                foodList.SubItems.Add(dr.GetValue(2).ToString())
                                foodList.SubItems.Add(dr.GetValue(3).ToString())
                                foodList.SubItems.Add(dr.GetValue(4).ToString())
                                foodList.SubItems.Add(dr.GetValue(5).ToString())
                            End While
                            Me.Hide()
                            frm1.Show()
                        End If
                    End With
                End With
            Else
                Return
            End If
        End If
    End Sub

    Private Sub BunifuFlatButton3_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton3.Click
        Me.Hide()
        LoginControl.Show()
    End Sub
End Class