﻿Imports MySql.Data.MySqlClient
Public Class AdminLogin

    Private Sub checkLength(password As String)
        If Not password.Length > 5 Then
            MessageBox.Show("must be more than 5 characters")
            TextBox1.Clear()

        End If
    End Sub

    Private Sub AdminLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox1.Select()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)
        Me.Hide()
        AdminSignUp.Show()
    End Sub

    Private Sub BunifuThinButton21_Click(sender As Object, e As EventArgs)
        Me.Hide()
        AdminSignUp.Show()
    End Sub

    Private Sub BunifuFlatButton1_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton1.Click
        Me.Hide()
        AdminSignUp.Show()
    End Sub

    Private Sub BunifuFlatButton2_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton2.Click
        If (TextBox1.Text IsNot "") And (TextBox2.Text IsNot "") Then

            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            mySqlConnection.Open()
            With cmd
                .Connection = mySqlConnection
                .CommandText = "SELECT * FROM tbladmin where username LIKE BINARY '" & TextBox1.Text & "' And password LIKE BINARY '" & TextBox2.Text & "' "
                dr = cmd.ExecuteReader()
                If dr.HasRows = True Then
                    While dr.Read()
                        stat = dr.GetValue(6).ToString()
                        If stat = "Active" Then
                            MessageBox.Show("Login Success")
                            Me.Hide()
                            LoginControl.Show()
                        Else
                            MessageBox.Show("Please ask the Administrator to Activate your account")
                        End If
                    End While
                Else
                    MessageBox.Show("Incorrect username or password")
                End If
            End With
        Else
            If (TextBox1.Text IsNot "") Then
                MsgBox("Password Required", vbCritical)
            ElseIf (TextBox2.Text IsNot "") Then
                MsgBox("Username Required", vbCritical)
            Else
                MsgBox("Username and Password Required", vbCritical)
            End If
        End If
    End Sub
End Class