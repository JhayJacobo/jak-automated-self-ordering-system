﻿Imports MySql.Data.MySqlClient
Public Class LoginControl
    Private Sub LoginControl_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If AdminLogin.TextBox1.Text = "admin" Then
            pos = dr.GetValue(1).ToString()

            If pos = "Admin" Then
                Panel2.Hide()
                Panel1.Show()
                Panel4.Show()
                Panel7.Show()
            Else
                Panel2.Show()
                Panel1.Hide()
                Panel4.Hide()
                Panel7.Show()
            End If
        Else
        End If
    End Sub

    Private Sub BunifuTileButton3_Click(sender As Object, e As EventArgs) Handles BunifuTileButton3.Click
        Dim reload As New OrderManagement
        Me.Hide()
        reload.Show()
    End Sub

    Private Sub BunifuFlatButton1_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton1.Click
        Dim result As DialogResult
        result = MessageBox.Show("Are you sure you want to Logout?", "Logout", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If result = DialogResult.Yes Then
            Me.Hide()
            Application.Restart()
        End If
    End Sub

    Private Sub BunifuTileButton1_Click_1(sender As Object, e As EventArgs) Handles BunifuTileButton1.Click
        Dim reload As New EmployeeManagement
        Me.Hide()
        reload.Show()
    End Sub

    Private Sub BunifuTileButton2_Click_1(sender As Object, e As EventArgs) Handles BunifuTileButton2.Click
        Dim reload As New CustomerManagement
        Me.Hide()
        reload.Show()
    End Sub

    Private Sub BunifuTileButton4_Click_1(sender As Object, e As EventArgs) Handles BunifuTileButton4.Click
        Dim reload As New AdminMenuEdit
        Me.Hide()
        reload.Show()
    End Sub

    Private Sub BunifuTileButton6_Click(sender As Object, e As EventArgs) Handles BunifuTileButton6.Click
        Dim reload As New ReportManagement
        Me.Hide()
        reload.Show()
    End Sub
End Class