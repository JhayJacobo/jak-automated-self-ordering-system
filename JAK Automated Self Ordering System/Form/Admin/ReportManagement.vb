﻿Imports MySql.Data.MySqlClient
Public Class ReportManagement

    Private Sub load_total()
        Try
            sql = "Select * from tblpaid"
            connect()
            cmd = New MySqlCommand(sql, con)
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            getTotal.Items.Clear()
            Dim x As ListViewItem

            Do While dr.Read = True
                x = New ListViewItem(dr("Total").ToString)
                getTotal.Items.Add(x)
            Loop

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cmd.Dispose()
            con.Close()
        End Try
    End Sub

    Private Sub load_name()
        Try
            sql = "Select * from tblcustomeraccounts"
            connect()
            cmd = New MySqlCommand(sql, con)
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            ListView1.Items.Clear()
            Dim x As ListViewItem

            Do While dr.Read = True
                x = New ListViewItem(dr("firstname").ToString)
                x.SubItems.Add(dr("lastname"))
                ListView1.Items.Add(x)
            Loop

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cmd.Dispose()
            con.Close()
        End Try
    End Sub

    Private Sub load_employeeTotal()
        Try
            sql = "Select * from tblfinalorder"
            connect()
            cmd = New MySqlCommand(sql, con)
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            EmployeeTotal.Items.Clear()
            Dim x As ListViewItem

            Do While dr.Read = True
                x = New ListViewItem(dr("name").ToString)
                x.SubItems.Add(dr("total"))
                EmployeeTotal.Items.Add(x)
            Loop

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cmd.Dispose()
            con.Close()
        End Try
    End Sub

    Private Sub load_credit()
        Try
            sql = "Select * from tblorder"
            connect()
            cmd = New MySqlCommand(sql, con)
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            ListView2.Items.Clear()
            Dim x As ListViewItem

            Do While dr.Read = True
                x = New ListViewItem(dr("queueID").ToString)
                x.SubItems.Add(dr("name"))
                x.SubItems.Add(dr("list"))
                x.SubItems.Add(dr("price"))
                x.SubItems.Add(dr("qty"))
                x.SubItems.Add(dr("sub"))
                x.SubItems.Add(dr("total"))
                ListView2.Items.Add(x)
            Loop

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cmd.Dispose()
            con.Close()
        End Try
    End Sub



    Private Sub BunifuTileButton1_Click(sender As Object, e As EventArgs) Handles BunifuTileButton1.Click
        Dim print As New SalesRep
        print.Load()
        print.Refresh()
        Dim reload As New SalesPrint
        reload.Show()
    End Sub

    Private Sub ReportManagement_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load_total()
        load_name()
        load_employeeTotal()
        load_credit()
        Dim Addtotal As Decimal
        Dim SaleTotal As Decimal
        For x As Integer = 0 To getTotal.Items.Count - 1
            Addtotal = getTotal.Items(x).SubItems(0).Text
            SaleTotal += Addtotal
        Next

        sql = "Update tblpaid set sales='" & SaleTotal & "'"
        connect()
        cmd = New MySqlCommand(sql, con)
        dr = cmd.ExecuteReader
        'MessageBox.Show(SaleTotal)

    End Sub

    Private Sub BunifuTileButton2_Click(sender As Object, e As EventArgs) Handles BunifuTileButton2.Click
        Dim first, last, useName As String
        If ListView1.SelectedItems.Count = 0 Then
            MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            first = ListView1.SelectedItems(0).SubItems(0).Text
            last = ListView1.SelectedItems(0).SubItems(1).Text
            useName = last + " " + first
            sql = "Update tblprint set name='" & useName & "'"
            connect()
            cmd = New MySqlCommand(sql, con)
            dr = cmd.ExecuteReader
            Dim reload As New EmployeeReportPrint
            reload.Show()

        End If
    End Sub

    Private Sub BunifuFlatButton3_Click(sender As Object, e As EventArgs)
        Dim reload As New LoginControl
        Me.Hide()
        reload.Show()
    End Sub

    Private Sub BunifuFlatButton1_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton1.Click
        Dim result As DialogResult
        result = MessageBox.Show("Are you sure you want to Delete this record? ", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If result = DialogResult.Yes Then
            Dim newmySqlConnection As MySqlConnection = StartMySqlConnection()
            newmySqlConnection.Open()
            With cmd
                .Connection = newmySqlConnection
                .CommandText = "Delete from tblorder"
                cmd.ExecuteNonQuery()
                'MessageBox.Show("Successfully record deleted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End With

            Dim exmySqlConnection As MySqlConnection = StartMySqlConnection()
            exmySqlConnection.Open()
            With cmd
                .Connection = exmySqlConnection
                .CommandText = "Delete from tblfinalorder"
                cmd.ExecuteNonQuery()
                'MessageBox.Show("Successfully record deleted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End With

            Dim mynewSqlConnection As MySqlConnection = StartMySqlConnection()
            mynewSqlConnection.Open()
            With cmd
                .Connection = mynewSqlConnection
                .CommandText = "Delete from tblpaid"
                cmd.ExecuteNonQuery()
                MessageBox.Show("Sales deleted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End With

            'Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            'mySqlConnection.Open()
            'With cmd
            '    .Connection = mySqlConnection
            '    .CommandText = "INSERT INTO tblpaid (`queueID`,`name`) VALUES (0,'.')"
            '    Dim mysql_result = cmd.ExecuteNonQuery
            'End With

            'Dim renewSqlConnection As MySqlConnection = StartMySqlConnection()
            'renewSqlConnection.Open()
            'With cmd
            '    .Connection = renewSqlConnection
            '    .CommandText = "Update tblpaid set queueID= 0"
            '    Dim mysql_result = cmd.ExecuteNonQuery
            'End With

            Me.Refresh()


        End If
    End Sub

    Private Sub ListView1_MouseClick(sender As Object, e As MouseEventArgs) Handles ListView1.MouseClick
        Dim first, last, useName As String
        EmployeeTotal.Items.Clear()
        Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
        mySqlConnection.Open()
        With cmd
            first = ListView1.SelectedItems(0).SubItems(0).Text
            last = ListView1.SelectedItems(0).SubItems(1).Text
            useName = last + " " + first
            .Connection = mySqlConnection
            .CommandText = "SELECT * FROM tblfinalorder where name LIKE '" & useName & "' "
            dr = cmd.ExecuteReader()
            If dr.HasRows = True Then
                While dr.Read()
                    foodList = EmployeeTotal.Items.Add(dr.GetValue(1).ToString())
                    foodList.SubItems.Add(dr.GetValue(2).ToString())

                End While
            End If

            Dim Addtotal As Decimal
            Dim SaleTotal As Decimal
            For x As Integer = 0 To EmployeeTotal.Items.Count - 1
                Addtotal = EmployeeTotal.Items(x).SubItems(1).Text
                SaleTotal += Addtotal
            Next

            sql = "Update tblorder set sales='" & SaleTotal & "'"
            connect()
            cmd = New MySqlCommand(sql, con)
            dr = cmd.ExecuteReader


        End With

        ListView2.Items.Clear()
        Dim newmySqlConnection As MySqlConnection = StartMySqlConnection()
        newmySqlConnection.Open()
        With cmd
            first = ListView1.SelectedItems(0).SubItems(0).Text
            last = ListView1.SelectedItems(0).SubItems(1).Text
            useName = last + " " + first
            .Connection = newmySqlConnection
            .CommandText = "SELECT * FROM tblorder where name LIKE '" & useName & "' "
            dr = cmd.ExecuteReader()
            If dr.HasRows = True Then
                While dr.Read()
                    foodList = ListView2.Items.Add(dr.GetValue(0).ToString())
                    foodList.SubItems.Add(dr.GetValue(1).ToString())
                    foodList.SubItems.Add(dr.GetValue(2).ToString())
                    foodList.SubItems.Add(dr.GetValue(3).ToString())
                    foodList.SubItems.Add(dr.GetValue(4).ToString())
                    foodList.SubItems.Add(dr.GetValue(5).ToString())
                End While
            End If
        End With
    End Sub

    Private Sub BunifuFlatButton2_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton2.Click
        Dim reload As New LoginControl
        Me.Hide()
        LoginControl.Show()

    End Sub

    Private Sub BunifuFlatButton4_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton4.Click
        Dim result As DialogResult
        Dim List As String
        Dim price As String
        Dim qty As String
        Dim subtotal As String
        Dim total As Decimal

        'If ListView1.SelectedItems.Count = 0 Then
        '    MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        'Else
        '    first = ListView1.SelectedItems(0).SubItems(0).Text
        '    last = ListView1.SelectedItems(0).SubItems(1).Text
        '    useName = last + " " + first
        '    sql = "Update tblprint set name='" & useName & "'"
        '    connect()
        '    cmd = New MySqlCommand(sql, con)
        '    dr = cmd.ExecuteReader
        '    Dim reload As New ReportManagement
        '    reload.Show()



        If ListView1.SelectedItems.Count = 0 Then
            MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            result = MessageBox.Show("Are you sure? ", "Paid", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then
                Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
                mySqlConnection.Open()
                With cmd
                    Dim first = ListView1.SelectedItems(0).SubItems(0).Text
                    Dim last = ListView1.SelectedItems(0).SubItems(1).Text
                    Dim useName = last + " " + first
                    For x As Integer = 0 To ListView2.Items.Count - 1
                        List += ListView2.Items(x).SubItems(2).Text
                        price += ListView2.Items(x).SubItems(3).Text
                        qty += ListView2.Items(x).SubItems(4).Text
                        subtotal += ListView2.Items(x).SubItems(5).Text
                    Next
                    If ListView2.Items.Count = 0 Then
                        MessageBox.Show("No Credit", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Else
                        For x As Integer = 0 To EmployeeTotal.Items.Count - 1
                            total += EmployeeTotal.Items(x).SubItems(1).Text
                        Next
                        'total = EmployeeTotal.Items(0).SubItems(1).Text
                        Dim queueID = ListView2.Items(0).SubItems(0).Text
                        .Connection = mySqlConnection
                        .CommandText = "INSERT INTO tblpaid (`queueID`,`name`,`list`,`price`,`qty`,`sub`,`Total`) VALUES (" & queueID & ",'" & useName & "','" & List & "','" & price & "','" & qty & "','" & subtotal & "', '" & total & "')"
                        Dim mysql_result = cmd.ExecuteNonQuery
                        If mysql_result = 0 Then
                            MsgBox("Process Failed", vbCritical)
                        Else
                            MessageBox.Show("Paid Success", "PAY", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Dim newSqlConnection As MySqlConnection = StartMySqlConnection()
                            newSqlConnection.Open()
                            With cmd
                                .Connection = newSqlConnection
                                .CommandText = "Delete from tblfinalorder where name = '" & useName & "'"
                                cmd.ExecuteNonQuery()
                                ListView2.Items.Clear()
                                con = StartMySqlConnection()
                                con.Open()
                            End With

                            Dim renewSqlConnection As MySqlConnection = StartMySqlConnection()
                            renewSqlConnection.Open()
                            With cmd
                                .Connection = renewSqlConnection
                                .CommandText = "Delete from tblorder where name = '" & useName & "'"
                                cmd.ExecuteNonQuery()
                                ListView2.Items.Clear()
                                con = StartMySqlConnection()
                                con.Open()
                            End With
                        End If
                    End If
                End With
            End If
        End If
    End Sub
End Class