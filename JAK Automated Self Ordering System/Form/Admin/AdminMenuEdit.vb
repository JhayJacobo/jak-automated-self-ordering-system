﻿Imports MySql.Data.MySqlClient
Imports System.IO
Public Class AdminMenuEdit
    Private Sub ListView1_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load_menuEdit()
    End Sub

    Private Sub load_menuEdit()
        Try
            sql = "Select * from tblMenu"
            connect()
            cmd = New MySqlCommand(sql, con)
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            ListView1.Items.Clear()
            Dim x As ListViewItem

            Do While dr.Read = True
                x = New ListViewItem(dr("foodID").ToString)
                x.SubItems.Add(dr("name"))
                x.SubItems.Add(dr("qty"))
                x.SubItems.Add(dr("price"))
                x.SubItems.Add(dr("meal"))
                x.SubItems.Add(dr("type"))

                ListView1.Items.Add(x)
            Loop

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cmd.Dispose()
            con.Close()
        End Try
    End Sub

    Private Sub ListView1_SelectedIndexChanged_1(sender As Object, e As EventArgs) Handles ListView1.SelectedIndexChanged
        If ListView1.SelectedItems.Count > 0 Then
            TextBox1.Text = ListView1.SelectedItems(0).SubItems(1).Text
            TextBox2.Text = ListView1.SelectedItems(0).SubItems(2).Text
            TextBox3.Text = ListView1.SelectedItems(0).SubItems(3).Text
            ComboBox1.Text = ListView1.SelectedItems(0).SubItems(4).Text
            ComboBox2.Text = ListView1.SelectedItems(0).SubItems(5).Text

        End If
    End Sub


    Private Sub BunifuFlatButton1_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton1.Click
        Dim frm1 As New AdminMenuEdit

        con.Close()

        Dim newmySqlConnection As MySqlConnection = StartMySqlConnection()
        newmySqlConnection.Open()
        With cmd
            .Connection = newmySqlConnection
            .CommandText = "SELECT * FROM tblmenu where name LIKE BINARY '%" & TextBox1.Text & "%'"
            dr = cmd.ExecuteReader()
            If dr.HasRows Then
                dr.Read()
                MessageBox.Show("Food already in list", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Hand)

            Else
                Try
                    If ((TextBox1.Text IsNot "") And (TextBox2.Text IsNot "") And (TextBox3.Text IsNot "") And (ComboBox1.Text IsNot "")) Then
                        con.Open()
                        sql = "INSERT into tblMenu(name,meal,type,qty,price) VALUES('" & TextBox1.Text & "','" & ComboBox1.Text & "','" & ComboBox2.Text & "','" & TextBox2.Text & "','" & TextBox3.Text & "')"
                        connect()
                        cmd = New MySqlCommand(sql, con)
                        cmd.ExecuteNonQuery()
                        MsgBox("Data Saved")
                        Me.Hide()
                        frm1.Show()
                    Else
                        MessageBox.Show("Please answer all forms", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    cmd.Dispose()
                    con.Close()
                End Try
            End If
        End With


    End Sub

    Private Sub BunifuFlatButton2_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton2.Click
        Dim frm1 As New AdminMenuEdit
        id = ListView1.SelectedItems(0).SubItems(0).Text
        Try
            If ListView1.SelectedItems.Count = 0 Then
                MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                sql = "Update tblMenu set name='" & TextBox1.Text & "', meal='" & ComboBox1.Text & "', type='" & ComboBox2.Text & "', qty='" & TextBox2.Text & "', price='" & TextBox3.Text & "' Where foodID = " & id & ""
                connect()
                cmd = New MySqlCommand(sql, con)
                dr = cmd.ExecuteReader
                MessageBox.Show("Menu Updated")
                Me.Hide()
                frm1.Show()
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cmd.Dispose()
            con.Close()
        End Try
    End Sub

    Private Sub BunifuFlatButton4_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub BunifuFlatButton4_Click_1(sender As Object, e As EventArgs) Handles BunifuFlatButton4.Click
        Dim result As DialogResult
        Dim reload As New AdminMenuEdit

        If ListView1.SelectedItems.Count = 0 Then
            MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            id = ListView1.SelectedItems(0).SubItems(0).Text
            result = MessageBox.Show("Are you sure you want to Delete this record? ", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then
                Dim newmySqlConnection As MySqlConnection = StartMySqlConnection()
                newmySqlConnection.Open()
                With cmd
                    .Connection = newmySqlConnection
                    .CommandText = "Delete from tblmenu where foodID = " & id & ""
                    cmd.ExecuteNonQuery()
                    MessageBox.Show("Successfully record deleted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ListView1.Items.Clear()
                    con = StartMySqlConnection()
                    con.Open()
                    With cmd
                        .Connection = con
                        .CommandText = "Select * from tblmenu"
                        dr = cmd.ExecuteReader
                        If dr.HasRows = True Then
                            While dr.Read
                                foodList = ListView1.Items.Add(dr.GetValue(0).ToString())
                                foodList.SubItems.Add(dr.GetValue(1).ToString())
                                foodList.SubItems.Add(dr.GetValue(2).ToString())
                                foodList.SubItems.Add(dr.GetValue(3).ToString())
                                foodList.SubItems.Add(dr.GetValue(4).ToString())
                                foodList.SubItems.Add(dr.GetValue(5).ToString())
                            End While
                            Me.Hide()
                            reload.Show()
                        End If
                    End With
                End With
            Else
                Return
            End If
        End If
    End Sub

    Private Sub BunifuFlatButton3_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton3.Click
        Me.Hide()
        LoginControl.Show()
    End Sub
End Class
