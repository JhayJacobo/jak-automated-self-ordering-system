﻿Imports MySql.Data.MySqlClient
Public Class OrderManagement
    Public foodList As New ListViewItem
    Public Newdr As MySqlDataReader
    Dim alltotal As Decimal
    Dim sum As Decimal
    Dim que As Integer
    Dim row_qty As Decimal
    Dim row_price As Decimal
    Dim qty_integer As Integer
    Dim price_integer As Decimal
    Dim counter As Integer
    Dim lastnameLabel As String
    Dim firstnameLabel As String

    Private Sub final_order()
        Try
            sql = "Select * from tblfinalorder"
            connect()
            cmd = New MySqlCommand(sql, con)
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            ListView3.Items.Clear()
            Dim x As ListViewItem

            Do While dr.Read = True
                x = New ListViewItem(dr("queue_id").ToString)
                x.SubItems.Add(dr("name"))
                x.SubItems.Add(dr("total"))

                ListView3.Items.Add(x)
            Loop

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cmd.Dispose()
            con.Close()
        End Try
    End Sub

    Private Sub load_order()
        Try
            sql = "Select * from tblorder where queueID > 0"
            connect()
            cmd = New MySqlCommand(sql, con)
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            orderList.Items.Clear()
            Dim x As ListViewItem

            Do While dr.Read = True
                x = New ListViewItem(dr("queueID").ToString)
                x.SubItems.Add(dr("name"))
                x.SubItems.Add(dr("list"))
                x.SubItems.Add(dr("price"))
                x.SubItems.Add(dr("qty"))
                x.SubItems.Add(dr("sub"))
                orderList.Items.Add(x)
            Loop

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cmd.Dispose()
            con.Close()
        End Try
    End Sub

    Private Sub load_menu()
        Try
            sql = "Select * from tblmenu where qty > 0"
            connect()
            cmd = New MySqlCommand(sql, con)
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            ListView1.Items.Clear()
            Dim x As ListViewItem

            Do While dr.Read = True
                x = New ListViewItem(dr("name").ToString)
                x.SubItems.Add(dr("meal"))
                x.SubItems.Add(dr("type"))
                x.SubItems.Add(dr("qty"))
                x.SubItems.Add(dr("price"))
                ListView1.Items.Add(x)
            Loop
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cmd.Dispose()
            con.Close()
        End Try
    End Sub

    'Private Sub auto_refresh()
    '    Dim timer As New Timer()
    '    timer.Interval = 1000

    '    '5 seconds
    '    AddHandler timer.Tick, AddressOf Timer1_Tick
    '    timer.Start()
    'End Sub


    Private Sub OrderManagement_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        final_order()
        load_order()
        load_menu()



    End Sub


    Private Sub Button6_Click(sender As Object, e As EventArgs)
        Dim result As DialogResult
        result = MessageBox.Show("Are you sure you want to Logout?", "Logout", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If result = DialogResult.Yes Then
            Me.Hide()
            Application.Restart()
        End If
    End Sub

    Private Sub ListView1_Click(sender As Object, e As EventArgs) Handles ListView1.Click
        Dim name, qty, price As String
        Dim IsItemExist As Boolean = False

        name = ListView1.Items(ListView1.FocusedItem.Index).SubItems(0).Text
        qty = ListView1.Items(ListView1.FocusedItem.Index).SubItems(3).Text
        price = ListView1.Items(ListView1.FocusedItem.Index).SubItems(4).Text

        Dim valuefromuser As Integer = +1

        valuefromuser = Convert.ToDecimal(valuefromuser)
            If valuefromuser > 0 Then

                For Each lv2_item As ListViewItem In ListView2.Items
                    Dim check_item As String
                    check_item = ListView2.Items(lv2_item.Index).SubItems(0).Text
                    If (name = check_item) Then
                        IsItemExist = True
                        ListView2.Items(lv2_item.Index).SubItems(1).Text = Convert.ToDecimal(ListView2.Items(lv2_item.Index).SubItems(1).Text) + (valuefromuser)
                    End If
                Next

                If (ListView2.Items.Count = 0) Then
                    'no order in table
                    Dim list_view_selected_item As New ListViewItem(name)
                    list_view_selected_item.SubItems.Add(valuefromuser)
                    list_view_selected_item.SubItems.Add(price)
                    list_view_selected_item.SubItems.Add(price * valuefromuser)
                    ListView2.Items.Add(list_view_selected_item)
                ElseIf (IsItemExist = False) Then
                    'item doesn't exist 
                    Dim list_view_selected_item As New ListViewItem(name)
                    list_view_selected_item.SubItems.Add(valuefromuser)
                    list_view_selected_item.SubItems.Add(price)
                    list_view_selected_item.SubItems.Add(price * valuefromuser)
                    ListView2.Items.Add(list_view_selected_item)
                End If
            End If

            Dim main_total As Decimal
            For Each item As ListViewItem In ListView2.Items
                row_qty = ListView2.Items(item.Index).SubItems(1).Text
                row_price = ListView2.Items(item.Index).SubItems(2).Text
                qty_integer = Decimal.Parse(row_qty)
                price_integer = Decimal.Parse(row_price)
                sum = qty_integer * price_integer
                ListView2.Items(item.Index).SubItems(3).Text = sum

                main_total += Convert.ToDecimal(ListView2.Items(item.Index).SubItems(3).Text)
            Next

            'alltotal += sum
            total_price_txt.Text = main_total
            alltotal = main_total

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim result As DialogResult
        Dim List As String
        Dim price As String
        Dim qty As String
        Dim subtotal As String
        Dim reload As New LoginControl
        Dim lv2_item_name, lv1_item_name As String
        Dim lv2_item_qty, lv1_item_qty As Integer
        If ((TextBox1.Text IsNot "") And (TextBox2.Text IsNot "")) Then
            For Each lv2_item As ListViewItem In ListView2.Items
                lv2_item_name = ListView2.Items(lv2_item.Index).SubItems(0).Text
                For Each lv1_item As ListViewItem In ListView1.Items
                    lv1_item_name = ListView1.Items(lv1_item.Index).SubItems(0).Text
                    If (lv2_item_name = lv1_item_name) Then

                        lv1_item_qty = ListView1.Items(lv1_item.Index).SubItems(3).Text
                        lv2_item_qty = ListView2.Items(lv2_item.Index).SubItems(1).Text
                    End If
                Next
            Next

            If ListView2.Items.Count = 0 Then
                MessageBox.Show("Please Select Order", "Order Select", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Else
                If (lv2_item_qty <= lv1_item_qty) Then
                    Dim lv2qty
                    For Each z As ListViewItem In ListView2.Items
                        lv2qty = ListView2.Items(z.Index).SubItems(1).Text
                        For Each x As ListViewItem In ListView1.Items
                            Dim renewSqlConnection As MySqlConnection = StartMySqlConnection()
                            renewSqlConnection.Open()
                            With cmd
                                .Connection = renewSqlConnection
                                Dim foodname = ListView1.Items(x.Index).SubItems(0).Text
                                Dim foodname2 = ListView2.Items(z.Index).SubItems(0).Text
                                Dim lv1qty = ListView1.Items(x.Index).SubItems(3).Text

                                If foodname = foodname2 Then
                                    Dim newqty As Integer = lv1qty - lv2qty
                                    .CommandText = "Update tblmenu set qty='" & newqty & "' Where name = '" & foodname & "'"
                                    Dim mysql_result = cmd.ExecuteReader
                                End If
                            End With
                        Next
                    Next

                    Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
                    mySqlConnection.Open()
                    With cmd
                        .Connection = mySqlConnection

                        .CommandText = "SELECT * FROM tblorder"
                        dr = cmd.ExecuteReader()
                        If dr.HasRows = True Then
                            dr.Read()
                            result = MessageBox.Show("Do you wish to proceed?", "Ask", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                            If result = DialogResult.Yes Then
                                con = StartMySqlConnection()
                                con.Open()
                                With cmd
                                    Name = TextBox2.Text + " " + TextBox1.Text
                                    For x As Integer = 0 To ListView2.Items.Count - 1
                                        List += ListView2.Items(x).SubItems(0).Text + vbNewLine
                                        qty += ListView2.Items(x).SubItems(1).Text + vbNewLine
                                        price += ListView2.Items(x).SubItems(2).Text + vbNewLine
                                        subtotal += ListView2.Items(x).SubItems(3).Text + vbNewLine
                                    Next
                                    price_total = alltotal
                                    .Connection = con
                                    .CommandText = "INSERT INTO tblpaid (`name`,`list`,`price`,`qty`,`sub`,`Total`) VALUES ('" & Name & "','" & List & "','" & price & "','" & qty & "','" & subtotal & "', '" & price_total & "')"
                                    Dim mysql_result = cmd.ExecuteNonQuery

                                    If mysql_result = 0 Then
                                        MsgBox("Data is NOT  Updated to MySQL Database", vbCritical)
                                    Else
                                        MsgBox("Order successfully", vbInformation)
                                        TextBox1.Clear()
                                        TextBox2.Clear()
                                        alltotal = 0
                                        ListView2.Items.Clear()
                                    End If
                                End With
                            End If
                        End If
                    End With





                Else
                    MessageBox.Show("should not exceed to Qty stocks", "STOCK WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            End If
        Else
            MessageBox.Show("Please input name", "NAME INPUT WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If ListView2.SelectedItems.Count = 0 Then
            MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Dim result As String
            result = MessageBox.Show("Are you sure you want to Delete this record? ", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then

                For Each item As ListViewItem In ListView2.SelectedItems

                    Dim minus As Integer
                    minus = ListView2.Items(item.Index).SubItems(3).Text
                    alltotal -= minus
                    total_price_txt.Text = alltotal
                    item.Remove()
                Next
            End If
        End If
    End Sub

    Private Sub ListView3_MouseClick(sender As Object, e As MouseEventArgs) Handles ListView3.MouseClick
        orderList.Items.Clear()
        id = ListView3.SelectedItems(0).SubItems(0).Text
        Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
        mySqlConnection.Open()
        With cmd
            .Connection = mySqlConnection
            .CommandText = "SELECT * FROM tblorder where queueID LIKE '" & id & "' "
            dr = cmd.ExecuteReader()
            If dr.HasRows = True Then
                While dr.Read()
                    foodList = orderList.Items.Add(dr.GetValue(0).ToString())
                    foodList.SubItems.Add(dr.GetValue(1).ToString())
                    foodList.SubItems.Add(dr.GetValue(2).ToString())
                    foodList.SubItems.Add(dr.GetValue(3).ToString())
                    foodList.SubItems.Add(dr.GetValue(4).ToString())
                    foodList.SubItems.Add(dr.GetValue(5).ToString())


                End While
            End If
        End With
    End Sub

    Private Sub BunifuFlatButton1_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton1.Click
        Dim result As DialogResult
        Dim reload As New OrderManagement
        Dim List As String
        Dim price As String
        Dim qty As String
        Dim subtotal As String
        If ListView1.Items.Count = 0 Then
            'Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            'mySqlConnection.Open()
            'With cmd
            '    .Connection = mySqlConnection
            '    .CommandText = "INSERT INTO tblorder (`queueID`,`name`) VALUES (0,'.')"
            '    Dim mysql_result = cmd.ExecuteNonQuery
            'End With

            Dim renewSqlConnection As MySqlConnection = StartMySqlConnection()
            renewSqlConnection.Open()
            With cmd
                .Connection = renewSqlConnection
                .CommandText = "Update tblorder set queueID= 0"
                Dim mysql_result = cmd.ExecuteNonQuery
            End With
        Else
            If ListView3.SelectedItems.Count = 0 Then
                MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                result = MessageBox.Show("Are you sure? ", "Paid", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If result = DialogResult.Yes Then
                    Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
                    mySqlConnection.Open()
                    With cmd
                        Dim queueid = ListView3.SelectedItems(0).SubItems(0).Text
                        Dim name = orderList.Items(0).SubItems(1).Text

                        For x As Integer = 0 To orderList.Items.Count - 1
                            List += orderList.Items(x).SubItems(2).Text
                            price += orderList.Items(x).SubItems(3).Text
                            qty += orderList.Items(x).SubItems(4).Text
                            subtotal += orderList.Items(x).SubItems(5).Text
                        Next

                        Dim total = ListView3.SelectedItems(0).SubItems(2).Text

                        .Connection = mySqlConnection
                        .CommandText = "INSERT INTO tblpaid (`queueID`,`name`,`list`,`price`,`qty`,`sub`,`Total`) VALUES (" & queueid & ",'" & name & "','" & List & "','" & price & "','" & qty & "','" & subtotal & "', '" & total & "')"
                        Dim mysql_result = cmd.ExecuteNonQuery
                        If mysql_result = 0 Then
                            MsgBox("Process Failed", vbCritical)
                        Else
                            MsgBox("Paid success")

                            id = ListView3.SelectedItems(0).SubItems(0).Text
                            Dim newSqlConnection As MySqlConnection = StartMySqlConnection()
                            newSqlConnection.Open()
                            With cmd
                                .Connection = newSqlConnection
                                .CommandText = "Delete from tblfinalorder where queue_id = " & id & ""
                                cmd.ExecuteNonQuery()
                                ListView3.Items.Clear()
                                con = StartMySqlConnection()
                                con.Open()
                                With cmd
                                    .Connection = con
                                    .CommandText = "Select * from tblfinalorder"
                                    dr = cmd.ExecuteReader
                                    If dr.HasRows = True Then
                                        While dr.Read
                                            queueList = orderList.Items.Add(dr.GetValue(0).ToString())
                                            queueList.SubItems.Add(dr.GetValue(1).ToString())
                                            queueList.SubItems.Add(dr.GetValue(2).ToString())

                                        End While
                                    End If

                                End With
                            End With

                            Dim mynewSqlConnection As MySqlConnection = StartMySqlConnection()
                            mynewSqlConnection.Open()
                            With cmd
                                .Connection = mynewSqlConnection
                                .CommandText = "Delete from tblorder where queueID = " & id & ""
                                cmd.ExecuteNonQuery()
                                orderList.Items.Clear()
                                con = StartMySqlConnection()
                                con.Open()
                                With cmd
                                    .Connection = con
                                    .CommandText = "Select * from tblorder"
                                    dr = cmd.ExecuteReader
                                    If dr.HasRows = True Then
                                        While dr.Read
                                            queueList = orderList.Items.Add(dr.GetValue(0).ToString())
                                            queueList.SubItems.Add(dr.GetValue(1).ToString())
                                            queueList.SubItems.Add(dr.GetValue(2).ToString())
                                            queueList.SubItems.Add(dr.GetValue(4).ToString())
                                            queueList.SubItems.Add(dr.GetValue(6).ToString())
                                        End While
                                    End If
                                    Me.Hide()
                                    reload.Show()
                                End With
                            End With
                        End If
                    End With
                End If
            End If

        End If
    End Sub

    Private Sub BunifuFlatButton2_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton2.Click
        Dim result As DialogResult
        Dim reload As New OrderManagement

        If ListView3.SelectedItems.Count = 0 Then
            MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            id = ListView3.SelectedItems(0).SubItems(0).Text
            result = MessageBox.Show("Are you sure you want to Delete this record? ", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then
                Dim newmySqlConnection As MySqlConnection = StartMySqlConnection()
                newmySqlConnection.Open()
                With cmd
                    .Connection = newmySqlConnection
                    .CommandText = "Delete from tblfinalorder where queue_id = " & id & ""
                    cmd.ExecuteNonQuery()
                    'MessageBox.Show("Successfully record deleted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ListView1.Items.Clear()
                    con = StartMySqlConnection()
                    con.Open()
                    With cmd
                        .Connection = con
                        .CommandText = "Select * from tblfinalorder"
                        dr = cmd.ExecuteReader
                        If dr.HasRows = True Then
                            While dr.Read
                                queueList = orderList.Items.Add(dr.GetValue(0).ToString())
                                queueList.SubItems.Add(dr.GetValue(1).ToString())
                                queueList.SubItems.Add(dr.GetValue(2).ToString())
                            End While
                        End If
                    End With
                End With

                Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
                mySqlConnection.Open()
                With cmd
                    .Connection = mySqlConnection
                    .CommandText = "Delete from tblorder where queueID = " & id & ""
                    cmd.ExecuteNonQuery()
                    MessageBox.Show("Successfully record deleted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ListView1.Items.Clear()
                    con = StartMySqlConnection()
                    con.Open()
                    With cmd
                        .Connection = con
                        .CommandText = "Select * from tblorder"
                        dr = cmd.ExecuteReader
                        If dr.HasRows = True Then
                            While dr.Read
                                queueList = orderList.Items.Add(dr.GetValue(0).ToString())
                                queueList.SubItems.Add(dr.GetValue(1).ToString())
                                queueList.SubItems.Add(dr.GetValue(2).ToString())
                                queueList.SubItems.Add(dr.GetValue(4).ToString())
                                queueList.SubItems.Add(dr.GetValue(6).ToString())
                            End While
                        End If
                        Me.Hide()
                        reload.Show()
                    End With
                End With
            Else
                Return
            End If
        End If
    End Sub

    Private Sub BunifuThinButton21_Click(sender As Object, e As EventArgs) Handles BunifuThinButton21.Click
        Dim result As DialogResult
        result = MessageBox.Show("Are you sure you want to Logout?", "Logout", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If result = DialogResult.Yes Then
            Application.Restart()
        End If
    End Sub

    Private Sub TextBox3_TextChanged(sender As Object, e As EventArgs) Handles TextBox3.TextChanged
        ListView1.Items.Clear()
        Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
        mySqlConnection.Open()
        With cmd
            .Connection = mySqlConnection
            .CommandText = "SELECT * FROM tblmenu where name LIKE '%" & TextBox3.Text & "%' and qty > 0"
            dr = cmd.ExecuteReader()
            If dr.HasRows = True Then
                While dr.Read()
                    foodList = ListView1.Items.Add(dr.GetValue(1).ToString())
                    foodList.SubItems.Add(dr.GetValue(2).ToString())
                    foodList.SubItems.Add(dr.GetValue(3).ToString())
                    foodList.SubItems.Add(dr.GetValue(4).ToString())
                    foodList.SubItems.Add(dr.GetValue(5).ToString())
                End While
            End If
        End With
    End Sub

    Private Sub BunifuFlatButton3_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton3.Click

        Me.Hide()
        Me.Hide()
        'pos = dr.GetValue(1).ToString()
        LoginControl.Show()
    End Sub


    Private Sub BunifuFlatButton4_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton4.Click
        Dim reload As New OrderManagement
        Me.Hide()
        reload.Show()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If ListView2.SelectedItems.Count = 0 Then
            MessageBox.Show("Please select one", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim result As String
            result = MessageBox.Show("Are you sure you want to Delete this record? ", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then

                For Each item As ListViewItem In ListView2.SelectedItems

                    Dim minus As Integer
                    minus = ListView2.Items(item.Index).SubItems(3).Text
                    alltotal -= minus
                    total_price_txt.Text = alltotal
                    item.Remove()
                Next
            End If
        End If
    End Sub
End Class