﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SalesPrint
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SalesRep3 = New JAK_Automated_Self_Ordering_System.SalesRep()
        Me.SalesRep1 = New JAK_Automated_Self_Ordering_System.SalesRep()
        Me.SalesRep2 = New JAK_Automated_Self_Ordering_System.SalesRep()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.SalesRep4 = New JAK_Automated_Self_Ordering_System.SalesRep()
        Me.SalesRep5 = New JAK_Automated_Self_Ordering_System.SalesRep()
        Me.SalesRep6 = New JAK_Automated_Self_Ordering_System.SalesRep()
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = 0
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Me.SalesRep6
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(662, 493)
        Me.CrystalReportViewer1.TabIndex = 0
        '
        'SalesPrint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(662, 493)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Name = "SalesPrint"
        Me.Text = "SalesPrint"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SalesRep1 As SalesRep
    Friend WithEvents SalesRep2 As SalesRep
    Friend WithEvents SalesRep3 As SalesRep
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents SalesRep4 As SalesRep
    Friend WithEvents SalesRep6 As SalesRep
    Friend WithEvents SalesRep5 As SalesRep
End Class
